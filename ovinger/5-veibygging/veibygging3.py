from sys import stdin
from math import log
from heapq import *

class UF:
    """Implementation of weighted quick-union with path compression"""

    def __init__(self, N):
        """Initialize data structure"""
        self.N = N
        self.id = [n for n in range(N)] # Use a dictionary, to support all data types
        self.sz = [1]*N # Ditto. This contains the size of the tree at p
        self.count = N

    def add(self, p):
        """Add site to data structure"""
        self.id[p] = p # A root site points to itself
        self.sz[p] = 1 # Component with a single site has size 1
        self.count += 1

    def union(self, p, q):
        """Add connection between p and q"""
        i = self.find(p)
        j = self.find(q)
        if i == j: return

        # Make smaller root point to larger one
        # This is the weighted union
        if self.sz[i] < self.sz[j]:
            self.id[i] = j
            self.sz[j] += self.sz[i]
        else:
            self.id[j] = i
            self.sz[i] += self.sz[j]

        self.count -= 1

    def find(self, q):
        """Return the root site of the component p belongs to"""
        # Copy the reference, so we don't change the input
        root = q

        # Find the root node
        while root != self.id[root]:
            root = self.id[root]

        # Find it again, this time changing the pointers to root.
        # This is the path compression
        #while q != root:
        #    q_copy = self.id[q]
        #    self.id[q] = root
        #    q = q_copy

        return root

    def connected(self, p, q):
        """Return true if p and q are in the same component"""
        return self.find(p) == self.find(q)

    def __repr__(self):
        return str(self.id.keys()) + "\n" + str(self.id.values())

Inf = float(1e3000)

# Nodes are implemented as a nested dict

class Graph:
    def __init__(self):
        self.N = 0
        self.neighbors = [] # Of dicts
        self.edges = []

    def mst_kruskal(self):
        """Returns the length of the most expensive edge in the MST"""
        uf = UF(self.N)
        self.edges.sort(reverse=True)
        longest = -Inf
        while uf.count > 1:
            edge = self.edges.pop()
            if not uf.connected(edge[1], edge[2]):
                uf.union(edge[1], edge[2])
                longest = max(longest, edge[0])
        return longest

    def mst_prim(self):
        added = set()
        pq = []
        longest = -Inf

        # Choose a node arbitrarily to begin from
        current = 0

        while 1:
            #print "Adding", current
            # Add current node to mst
            added.add(current)

            # Add its edges to the PQ
            for neighbor, weight in self.neighbors[current].iteritems():
                if neighbor not in added:
                    #pq.add((weight, current, neighbor))
                    heappush(pq, (weight, current, neighbor))

            if len(added) == self.N:
                break

            # Choose next edge as the minimum in the pq
            edge = heappop(pq)
            longest = max(longest, edge[0])
            current = edge[2]
            
        return longest

G = Graph()
node = 0
E = 0
for line in stdin:
    G.neighbors.append({})
    for edge in line.split():
        E += 1
        # Parse edge
        neighbor, weight = edge.split(":")
        neighbor, weight = int(neighbor), int(weight)
        # Add edge to neighbor list (for prims)
        G.neighbors[node][neighbor] = int(weight)
        # Add edge to edge list (for kruskal)
        G.edges.append((weight, node, neighbor))
        
    node += 1
    G.N += 1

V = node + 1
E = E // 2 # Because they go both ways
D = float(2*E)/V**2

if D > log(V, 2):
    print G.mst_prim()
else:
    print G.mst_kruskal()



