# -*- coding: utf8 -*-
from sys import stdin

def sorter(A):
    # Merk: den sorterte lista må returneres
    # START IKKE-UTDELT KODE
    for j in xrange(1, len(A)):
        k = A[j]
        i = j - 1
        while i >= 0 and A[i] > k:
            A[i+1] = A[i]
            i = i - 1
        A[i+1] = k
    return A
    # SLUTT IKKE-UTDELT KODE

def finn(A, min, maks):
    # Merk: resultatet må returneres
    # START IKKE-UTDELT KODE
    indeks_min = binsok(A, min)
    indeks_maks = binsok(A, maks)
    if A[indeks_min] < min:
        indeks_min += 1
    if A[indeks_maks] > maks:
        indeks_maks -= 1

    if A[indeks_maks] < A[indeks_min]:
        return None
    else:
        return [A[indeks_min], A[indeks_maks]]

def binsok(A, verdi):
    l = 0
    r = len(A) - 1
    while l <= r:
        m = (l + r) // 2
        if verdi == A[m]:
            break
        elif verdi < A[m]:
            r = m - 1
        else:
            l = m + 1
    return m
# SLUTT IKKE-UTDELT KODE

liste = []
for x in stdin.readline().split():
    liste.append(int(x))

sortert = sorter(liste)

#print l
stdin = ["5 40", "0 10", "15 99", "0 1000", "0 0", "140 140"]

for linje in stdin:
    ord = linje.split()
    min = int(ord[0])
    maks = int(ord[1])
    resultat = finn(sortert, min, maks)
    if resultat == None:
        print
    else:
        print min, maks, "-->", str(resultat[0]) + " " + str(resultat[1])
