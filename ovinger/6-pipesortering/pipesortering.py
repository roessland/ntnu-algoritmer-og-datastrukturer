from sys import stdin

def quicksort(l, shuffle_list=True):
    if not l:
        return l
    v = l[0]
    left = [n for n in l[1:] if n <= v]
    right = [n for n in l[1:] if n > v]
    return quicksort(left, False) + [v] + quicksort(right, False)

def binary_search(l, n, lo, hi):
    if hi <= lo:
        return lo
    mid = (lo + hi) // 2
    if l[mid] == n:
        return mid
    if l[mid] < n:
        return binary_search(l, n, mid + 1, hi)
    else:
        return binary_search(l, n, lo, mid - 1)

def interval(l, down, up):
    hi = len(l) - 1

    lower = binary_search(l, down, 0, hi)
    upper = binary_search(l, up, 0, hi)         

    return l[lower], l[upper]


l = quicksort([int(n) for n in stdin.readline().split()])
#print l
stdin = ["5 40", "0 10", "15 99", "0 1000", "0 0", "140 140"]
for line in stdin:
    nums = [int(n) for n in line.split()]
    i = interval(l, nums[0], nums[1])
    print nums[0], nums[1], "-->",
    if i:
        print i[0], i[1]
    else:
        print 
