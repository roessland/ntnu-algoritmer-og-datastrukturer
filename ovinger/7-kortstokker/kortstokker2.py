# -*- coding: utf8 -*-
from sys import stdin
from itertools import repeat

def mergelists(a, b):
    c = []
    len_a = len(a)
    len_b = len(b)
    n_a = 0
    n_b = 0
    while n_a < len_a and n_b < len_b:
        if a[n_a] < b[n_b]:
            c.append(a[n_a])
            n_a += 1
        else:
            c.append(b[n_b])
            n_b += 1
    if n_a == len(a):
        c.extend(b[n_b:])
    else:
        c.extend(a[n_a:])
    return c

def splitsort(decks):
    n = len(decks)
    if n == 1:
        return decks[0]
    if n == 2:
        return mergelists(decks[0], decks[1])
    else:
        return mergelists(splitsort(decks[:n//2]), splitsort(decks[n//2:]))
        

def merge(decks):
    return "".join([char for index, char in splitsort(decks)])
    # Legg ting i et dictionary
    

decks = []
for line in stdin:
    (index, list) = line.split(':')
    deck = zip(map(int, list.split(',')), repeat(index))
    decks.append(deck)
print merge(decks)

