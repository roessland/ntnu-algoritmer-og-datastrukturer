from sys import stdin

Inf = 1e10

def memoizeCoins(function):
    cache = {}
    def decorated(coins, value):
        if value not in cache:
            cache[value] = function(coins, value)
        return cache[value]
    return decorated

# Decorator works here too, but greedy function is already extremely fast
def minCoinsGreedy(coins, value):
    coin_amount = 0
    for coin in coins:
        modulus, value = divmod(value, coin)
        coin_amount += modulus
    return coin_amount

@memoizeCoins
def minCoinsDynamic(coins, value):
    if value == 0:
        return 0
    coin_amount = 0
    return 1 + min([minCoinsDynamic(coins, value - coin) for coin in coins if value >= coin])

def canUseGreedy(coins):
    return False




coins = []
for c in stdin.readline().split():
    coins.append(int(c))
coins.sort(reverse=True)
method = stdin.readline().strip()
if method == "graadig" or (method == "velg" and canUseGreedy(coins)):
    for line in stdin:
        print minCoinsGreedy(coins, int(line))
else:
    for line in stdin:
        print minCoinsDynamic(coins, int(line))
