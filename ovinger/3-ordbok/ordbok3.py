from sys import stdin

class Node(object):
    def __init__(self):
        self.positions = []
        self.children = {}

class Dawg(object):
    def __init__(self):
        self.root = Node()

    def insert(self, word, position):
        self._insert(self.root, word, position, 0)

    def _insert(self, node, word, position, index):
        if index == len(word):
            # At final node, add position
            node.positions.append(position)
        else:
            # Create next node if it is missing
            if word[index] not in node.children:
                node.children[word[index]] = Node()
            # And then insert remaining part of word in that node
            self._insert(node.children[word[index]], word, position, index + 1)

    def search(self, word):
        #print "search(" + word + ")"
        return sorted(self._search(self.root, word, 0))

    def _search(self, node, word, index):
        #print "_search(" + str(node) + ", " + word + ", " + str(index) + ")"
        if index == len(word):
            # At final node, return positions
            return node.positions
        else:
            if word[index] in node.children:
                return self._search(node.children[word[index]], word, index + 1)
            elif word[index] == '?':
                positions = []
                for child in node.children.values():
                    positions.extend(self._search(child, word, index + 1))
                return positions
            else:
                return []

                
# Get the words
words = stdin.readline().split()
words_count = len(words)

# Find indexes, and insert words into the dawg
dawg = Dawg()
word_num = 0
position = 0
while word_num < words_count:
    dawg.insert(words[word_num], position)
    position += len(words[word_num]) + 1
    word_num += 1


# Search trough the Dawg for each remaing line in stdin
for line in stdin:
    word = line[:-1]
    print word + ":",
    for position in dawg.search(word):
        print position,
    print

#print "root:", id(dawg.root)
