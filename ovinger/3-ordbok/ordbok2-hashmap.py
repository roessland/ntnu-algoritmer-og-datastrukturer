# -*- coding: utf-8 -*-
from sys import stdin, stderr
import traceback

hashmap = {} 

class Node:
    def __init__(self):
        self.children = {}
        self.positions = []

def build(wordlist):
    root = Node()

    # Gå gjennom alle ordene
    for word, position in wordlist:

        if word in hashmap:
            hashmap[word] += [position]
        else:
            hashmap[word] = [position]

        current_node = root
        # Gå gjennom ordets noder, og legg dem til hvis de mangler
        for char in word:
            # Hvis noden ikke har denne bokstaven etter seg
            if char not in current_node.children:
                # Lag en ny node med denne bokstaven
                current_node.children[char] = Node()
            # Gå til neste node
            current_node = current_node.children[char]

        # Vi er på den siste bokstaven i ordet, så lagre posisjonen
        current_node.positions.append(position)

    # Returner rot-noden
    return root
    

def positions(word, index, node, wordlen):
    """Returns all the positions where a word exists"""

    # If this is the first function, check the hashmap
    if index == 0:
        if word in hashmap:
            return hashmap[word]

    if wordlen == index:
        # Vi har funnet ordet. Returner posisjonen
        return node.positions
    else:
        # Har ikke funnet det enda, returner svaret fra neste node
        if word[index] == '?':
            results = []
            # Gå gjennom children-nodene til denne
            for child in node.children.values():
                try:
                    results += positions(word, index + 1, child, wordlen)
                except:
                    return []
            return results
        try:
            return positions(word, index + 1, node.children[word[index]], wordlen)
        except:
            return []

try:
    # Les inn ordene i ordlisten
    words = stdin.readline().split()
    wordlist = []
    pos = 0
    
    # Legg til ord i ordlisten, med tilhørende posisjoner
    for word in words:
        wordlist.append( (word, pos) )
        pos += len(word) + 1

    # Lag DAWG-en
    root = build(wordlist)
    # Gå gjennom søkeordene
    for searchword in stdin:
        searchword = searchword.strip()
        print searchword + ":",
        # Finn alle posisjonene ordet er på
        positions_list = positions(searchword, 0, root, len(searchword))
        # Print dem ut
        positions_list.sort()
        for position in positions_list:
            print position,
        print # newline
except:
    traceback.print_exc()



