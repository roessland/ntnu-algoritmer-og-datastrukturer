# -*- coding: utf-8 -*-
from sys import stdin
from os import _exit

#import cProfile
#cProfile.run("velg()")

# Finn hvilken funksjon som skal kjøres
funksjon = stdin.readline().strip()
antall_noder = int(stdin.readline()) # N

def velg():
    _int = int
    start_node = stdin.readline().rstrip()
    ratat_node = stdin.readline().rstrip()
    parent = {}
    parent[start_node] = start_node
    
    this_parent = None
    for line in stdin:
        first = True
        for n_str in line.split():
            # Hvis forste runden, lagre parent
            if first:
                first = False
                this_parent = n_str
                continue
            # Resten av rundene, gi dem en parent
            parent[n_str] = this_parent
          
    depth = 0
    while 1: 
        ratat_node = parent[ratat_node]
        depth += 1
        if ratat_node == start_node:
            print depth
            exit()

if funksjon == "velg":
    velg()
    exit()



class Node:
    dybde = None
    children = None 
    ratatosk = None
    depth = None
    nesteBarn = None # bare til bruk i DFS
    def __init__(self):
        self.children = []
        self.ratatosk = False
        self.nesteBarn = 0

def dfs(rot):
    stack = [rot]
    while stack:
        # Hent den øverste noden i bunken
        current_node = stack.pop()

        # Se etter ratatosk i den
        if current_node.ratatosk:
            return current_node.depth

        # Legg barna til noden i stacken, og lagre dybden
        for child in current_node.children:
            child.depth = current_node.depth + 1
            stack.append(child)
        
def bfs(rot):
    queue = [rot]
    while len(queue):
        # Hent den første noden i køen
        current_node = queue.pop(0)

        # Se etter ratatosk i den
        if current_node.ratatosk:
            return current_node.depth

        # Legg barna til noden i køen, og lagre dybden
        for child in current_node.children:
            child.depth = current_node.depth + 1
            queue.append(child)

if funksjon == "dfs" or funksjon == "bfs":
    # Lag N tomme noder
    noder = []
    for i in range(antall_noder):
        noder.append(Node())

    # Sett start-noden
    start_node = noder[int(stdin.readline())]
    start_node.depth = 0

    # Sett ratatosk-noden
    ratatosk_node = noder[int(stdin.readline())]
    ratatosk_node.ratatosk = True

    for linje in stdin:
        tall = linje.split() # parent_nr barn_nr1 barn_nr2 ...
        parent = noder[int(tall.pop(0))] # Parent-noden hentes fra listen
        for barn_nr in tall:
            # Legg barna til parent
            parent.children.append(noder[int(barn_nr)])

if funksjon == 'dfs':
    print dfs(start_node)
elif funksjon == 'bfs':
    print bfs(start_node)


