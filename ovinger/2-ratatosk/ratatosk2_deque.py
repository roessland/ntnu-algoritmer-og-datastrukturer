from sys import stdin
from collections import deque


class Node:
    dybde = None
    children = None 
    ratatosk = None
    depth = None
    nesteBarn = None  # bare til bruk i DFS
    def __init__(self):
        self.children = []
        self.ratatosk = False
        self.nesteBarn = 0


def dfs(rot):
    stack = [rot]
    while stack:
        # Hent den overste noden i bunken
        current_node = stack.pop()

        # Se etter ratatosk i den
        if current_node.ratatosk:
            return current_node.depth

        # Legg barna til noden i stacken, og lagre dybden
        for child in current_node.children:
            child.depth = current_node.depth + 1
            stack.append(child)


def bfs(rot):
    queue = deque(rot)
    while queue:
        # Hent den forste noden i queuen
        current_node = queue.popleft()

        # Se etter ratatosk i den
        if current_node.ratatosk:
            return current_node.depth

        # Legg barna til noden i queuen, og lagre dybden
        for child in current_node.children:
            child.depth = current_node.depth + 1
            queue.append(child)

# Finn hvilken funksjon som skal kjores
funksjon = stdin.readline().strip()
antall_noder = int(stdin.readline()) # N

# Lag N tomme noder
noder = []
for i in range(antall_noder):
    noder.append(Node())

# Sett start-noden
start_node = noder[int(stdin.readline())]
start_node.depth = 0

# Sett ratatosk-noden
ratatosk_node = noder[int(stdin.readline())]
ratatosk_node.ratatosk = True

for linje in stdin:
    tall = linje.split()  # parent_nr barn_nr1 barn_nr2 ...
    parent = noder[int(tall.pop(0))]  # Parent-noden hentes fra listen
    for barn_nr in tall:
        # Legg barna til parent
        parent.children.append(noder[int(barn_nr)])

if funksjon == 'dfs':
    print dfs(start_node)
elif funksjon == 'bfs':
    print bfs(start_node)
elif funksjon == 'velg':
    print bfs(start_node)

