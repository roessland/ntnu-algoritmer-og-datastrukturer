# -*- coding: utf8 -*-
from sys import *
import traceback

def subgraftetthet(nabomatrise, startnode):
    #print "starter paa", startnode
    N = len(nabomatrise)
    alle_noder = set(range(0, N))
    besokte_noder = set()

    # DFS, og putt alle besøkte noder i settet
    stakk = [startnode]

    while stakk:
        # Hent en node fra stakken
        denne = stakk.pop()
        # Legg denne noden til i besøkte noder
        besokte_noder.add(denne)
        #print "Besokte", denne
        # Legg alle nabonoder til denne i stakken
        for n in range(0, N):
            # Men bare hvis de ikke er besøkt, eller skal besøkes
            if nabomatrise[denne][n] and (n not in besokte_noder) and (n not in stakk) :
                #print "La til", n, "siden den ikke er besokt. besokte: ", besokte_noder
                stakk.append(n)

    subgraf = alle_noder - besokte_noder
    
    # Gå gjennom subgrafen og tell antall kanter innad
    kanter = 0
    for node in subgraf:
        for n in range(0, N):
            if nabomatrise[node][n] and n in subgraf:
                #print node, "er nabo med", n, "i subgrafen"
                kanter += 1

    noder = len(subgraf)
    if noder == 0:
        return 0.0
    else:
        return float(kanter) / noder**2

try:
    # En n x n kvadratisk matrise
    n = int(stdin.readline())
    nabomatrise = [None] * n # rader

    # Gå gjennom radene
    for i in range(0, n):
        # Alloker en kolonne med plass
        nabomatrise[i] = [False] * n
        linje = stdin.readline()
        # Gå gjennom denne kolonnen
        for j in range(0, n):
            # Sett verdien til sann hvis den er 1 i matrisen
            nabomatrise[i][j] = (linje[j] == '1')
    
    # Hent startnodene, og print output for dem
    for linje in stdin:
        start = int(linje)
        print "%.3f" % subgraftetthet(nabomatrise, start)
except:
    traceback.print_exc(file=stderr)
