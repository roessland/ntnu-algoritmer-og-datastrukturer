# -*- coding: utf8 -*-
from sys import stdin

# Input:
#   1. linje: antall noder
#   n. linjer: uvektet nabomatrise
#   resten: startnoder for traverseringen
# Output:
#   For hver startnode, en linje med subgraftettheten,
#   hvor subgrafen er den delen av grafen som ikke kan nås ved traversering
#   startet fra startnoden.



def subgraftetthet(naboset, startnode):
    noder = n = len(naboset)
    all_nodes = set(range(0, n))
    visited_nodes = set()

    # Traverser for å finne tilgjengelige noder
    # Verken DFS eller BFS fordi iterasjonsrekkefølgen er udefinert for set
    stack = set([startnode])
    while stack:
        current = stack.pop() # Set støtter faktisk pop
        visited_nodes.add(current)
        # Gå gjennom alle naboene
        for neighbor in naboset[current]:
            # Som ikke er besøkt, eller skal besøkes
            if neighbor not in visited_nodes and \
               neighbor not in stack:
                # Og legg dem i settet av noder som skal besøkes
                stack.add(neighbor)

    # Sets er gode til dette
    subgraph = all_nodes - visited_nodes
    
    # Gå gjennom subgrafen, og tell antall kanter som ikke leder ut av den
    kanter = 0
    for node in subgraph:
        for neighbor in naboset[node]:
            if neighbor in subgraph:
                kanter += 1
                
    # Antall noder i subgrafen er enkelt å finne
    noder = len(subgraph)

    if noder == 0:
        return 0.0
    else:
        return float(kanter) / float(noder**2)
    return 0.0


# Antall noder i grafen
n = int(stdin.readline())

# Lager en liste med nabo-set.
# Eks: Hvis naboset[2] = set(0, 1, 4), så går den en kant fra 2 til 0, 1 og 4.
naboset = [set() for m in range(0, n)]

for i in range(0, n):
    linje = stdin.readline()
    for j in range(0, n):
        if linje[j] == '1':
            naboset[i].add(j)



# Går gjennom resten av input, og skriver ut tettheten til subgrafen som finnes
# ved å starte i diverse noder
for linje in stdin:
    start = int(linje)
    print "%.3f" % (subgraftetthet(naboset, start) + 1E-12)
