# -*- coding: utf8 -*-
from sys import stdin
from copy import copy

# Input:
#   1. linje: antall noder
#   n. linjer: uvektet nabomatrise
#   resten: startnoder for traverseringen
# Output:
#   For hver startnode, en linje med subgraftettheten,
#   hvor subgrafen er den delen av grafen som ikke kan nås ved traversering
#   startet fra startnoden.



def dfs(naboset, start):
    """Returns a set of nodes reachable from startnode"""
    # dfs.cache
    # dfs.visited
    #print "Searching from", start
    if dfs.cache[start]:
        #print "Used cache starting at", start, ":", dfs.cache[start]
        return dfs.cache[start]
    
    stack = set([start])
    visited_nodes = set()
    while stack:
        current = stack.pop()
        visited_nodes.add(current)
        #print "     Currently at", current, ". Visited:", visited_nodes
        for neighbor in naboset[current]:
            if neighbor not in visited_nodes and neighbor not in stack:
                #print "         Cache for neighbor", neighbor, "is", dfs.cache[neighbor]
                if dfs.cache[neighbor]:
                    #print "         Using cache for", neighbor, "."
                    visited_nodes |= dfs.cache[neighbor]
                    #print "         Now visited:", visited_nodes
                else:
                    #print "         Not using cache for neighbor", neighbor
                    stack.add(neighbor)
                
    dfs.cache[start] = visited_nodes
    #print "Setting cache for", start, "to", visited_nodes
    return visited_nodes
        


def subgraftetthet(naboset, startnode):
    noder = n = len(naboset)
    all_nodes = set(range(0, n))

    # Sets er gode til dette
    subgraph = all_nodes - dfs(naboset, startnode)
    
    # Gå gjennom subgrafen, og tell antall kanter som ikke leder ut av den
    kanter = 0
    for node in subgraph:
        for neighbor in naboset[node]:
            if neighbor in subgraph:
                kanter += 1
                
    # Antall noder i subgrafen er enkelt å finne
    noder = len(subgraph)

    if noder == 0:
        return 0.0
    else:
        return float(kanter) / float(noder**2)
    return 0.0


# Antall noder i grafen
n = int(stdin.readline())

# Lager en liste med nabo-set.
# Eks: Hvis naboset[2] = set(0, 1, 4), så går den en kant fra 2 til 0, 1 og 4.
naboset = [set() for m in range(0, n)]

for i in range(0, n):
    linje = stdin.readline()
    for j in range(0, n):
        if linje[j] == '1':
            naboset[i].add(j)

dfs.cache = [set() for m in range(0, n)]

# Går gjennom resten av input, og skriver ut tettheten til subgrafen som finnes
# ved å starte i diverse noder
for linje in stdin:
    start = int(linje)
    print "%.3f" % (subgraftetthet(naboset, start) + 1E-12)

#dfs(naboset, 0)

