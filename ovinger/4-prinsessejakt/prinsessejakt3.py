# -*- coding: utf8 -*-
from sys import stdin

def subgraftetthet(nabomatrise, startnode):
    noder = n = len(nabomatrise)
    all_nodes = set(range(0, n))
    visited_nodes = set()

    # Traverser for å finne tilgjengelige noder
    # Ikke DFS eller BFS grunnet bruk av set
    stack = set([startnode])
    while stack:
        current = stack.pop() # Set støtter faktisk pop
        visited_nodes.add(current)
        for neighbor in all_nodes:
            if nabomatrise[current][neighbor] and \
               neighbor not in visited_nodes and \
               neighbor not in stack:
                stack.add(neighbor)

    subgraph = all_nodes - visited_nodes
    # Gå gjennom subgrafen, og tell antall kanter som ikke leder ut av den
    kanter = 0
    for node in subgraph:
        for neighbor in all_nodes:
            if nabomatrise[node][neighbor] and neighbor in subgraph:
                kanter += 1

    noder = len(subgraph)
    if noder == 0:
        return 0.0
    else:
        return float(kanter) / float(noder**2)
    return 0.0



n = int(stdin.readline())
nabomatrise = [None] * n # rader
kanter = 0
for i in range(0, n):
    nabomatrise[i] = [False] * n # kolonner
    linje = stdin.readline()
    for j in range(0, n):
        if linje[j] == '1':
            nabomatrise[i][j] = True
            kanter += 1
for linje in stdin:
    start = int(linje)
    print "%.3f" % (subgraftetthet(nabomatrise, start) + 1E-12)
