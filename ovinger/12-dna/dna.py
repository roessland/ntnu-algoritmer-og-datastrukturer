def levenshtein_distance(s, t, cache):
    """
    Calculates the minimum edit distance from the string s to t,
    using the following operations:
        * Insertion
        * Deletion
        * Substitution

    >>> cache = {}
    >>> levenshtein_distance('', '', cache)
    0
    >>> levenshtein_distance('A', '', cache)
    1
    >>> levenshtein_distance('A', 'A', cache)
    0
    >>> levenshtein_distance('A', 'B', cache)
    1
    >>> levenshtein_distance('AA', 'BB', cache)
    2
    >>> levenshtein_distance('A', 'AAA', cache)
    2
    >>> levenshtein_distance('ATGCATGC', 'TGCATGC', cache)
    1
    >>> levenshtein_distance('ABCDEFGH', 'ABCEFGH', cache)
    1
    >>> levenshtein_distance('fdsfsdaffesfesfdsfdsafesafdsfesafdsfdsa','fdsafvdvfdgrsafeasfesfesfdsafesafdsafdsafeafesf', cache)
    16
    """
    # Make s, t alphabetical, since levenshtein distance is commutative,
    # to avoid duplicate caching of swapped strings.
    if s > t:
        s, t = t, s

    # Check cache
    if s in cache and t in cache[s]:
        return cache[s][t]

    len_s = len(s)
    len_t = len(t)

    if len_s == 0: return len_t
    if len_t == 0: return len_s

    if s[0] == t[0]:
        cost = 0
    else:
        cost = 1
    
    distance =  min(levenshtein_distance(s[1:len_s], t, cache) + 1,
                    levenshtein_distance(s, t[1:len_t], cache) + 1,
                    levenshtein_distance(s[1:len_s], t[1:len_t], cache) + cost)
    
    # Store in cache
    if s not in cache:
        cache[s] = {}
    cache[s][t] = distance

    return distance

def least_distance_sum(strings):
    cache = {}
    len_strings = len(strings)
    distance_sum = [0] * len_strings
    for i in range(0, len_strings):
        for j in range(0, len_strings):
            if i != j:
                distance_sum[i] += levenshtein_distance(strings[i], strings[j], cache)

    return min(distance_sum)
                


from sys import stdin
strings = []
for line in stdin:
    strings.append(line.strip())
print least_distance_sum(strings)
