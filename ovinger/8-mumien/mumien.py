# -*- coding: utf8 -*-
from sys import stdin
from collections import deque

# antall noder
# sannsynligheter
# koblinger * N

N = int(stdin.readline())
prob = [float(x) for x in stdin.readline().split()]
safety = [0.0] * N
safety[0] = prob[0]
previous = range(N)

graph = [set() for x in range(0, N)]

# Bygg grafen
current = 0
for line in stdin:
    for neighbor in (int(x) for x in line.split()):
        graph[current].add(neighbor)
    current += 1

# BFS
queue = deque([0])
while queue:
    current = queue.popleft()
    for neighbor in graph[current]:
        new_safety = safety[current] * prob[neighbor]
        if new_safety > safety[neighbor]:
            safety[neighbor] = new_safety
            queue.append(neighbor)
            previous[neighbor] = current

# Finn veien tilbake
if safety[N-1] == 0.0:
    print 0
else:
    way = deque([str(N-1)])
    current = N-1
    while current != 0:
        current = previous[current]
        way.appendleft(str(current))

    print "-".join(way)
