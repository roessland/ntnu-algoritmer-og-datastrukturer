Inf = 10e3000

def matmult(A, B):
    transp_B = zip(*B)
    return [[sum(ele_A * ele_B for ele_A, ele_B in zip(row_A, col_B)) for col_B in transp_B] for row_A in A]

def extend_shortest_paths(L, W):
    Inf = 10e3000
    n = len(L)
    M = [[Inf] * n for _ in range(n)]
    for i in range(0, n):
        for j in range(0, n):
            for k in range(0, n):
                M[i][j] = min(M[i][j], L[i][k] + W[k][j])
    return M

def extend_shortest_paths_2(L, W):
    Inf = 10e3000
    n = len(L)
    return [[min([L[i][j]].extend([L[i][k] + W[k][j] for k in range(0, n)])) for j in range(0, n)] for i in range(0, n)]

def faster_all_pairs_shortest_path(W):
    n = len(W)
    m = 1
    Lm = W
    while m < n - 1:
        m = 2*m
        Lm = extend_shortest_paths(Lm, Lm)
    return Lm



W =  [[0, 3, 8, Inf, -4], [Inf, 0, Inf, 1, 7], [Inf, 4, 0, Inf, Inf], [2, Inf, -5, 0, Inf], [Inf, Inf, Inf, 6, 0]]

#print faster_all_pairs_shortest_path(W)
print extend_shortest_paths_2(W, W)
