from sys import stdin

class Kubbe:
    vekt = None
    neste = None
    def __init__(self, vekt):
        self.vekt = vekt
        self.neste = None

def spor(kubbe):
    if kubbe == None:
        return
    return max(kubbe.vekt, spor(kubbe.neste))


# Opprett lenket liste
forste = None
siste = None

for linje in stdin:
    forrige_siste = siste
    siste = Kubbe(int(linje))
    if forste == None:
        forste = siste
    else:
        forrige_siste.neste = siste

print spor(forste)
