# -*- coding: utf-8 -*-
class Topological_sort:
    """
    Sorterer en graf topologisk, ut fra en startnode. Kun nodene som trengs
    kommer med i resultatlisten.

    Eksempel på hvordan man kan finne en rekkefølge å ta på klærne, hvis målet
    er å ta på en bukse:
    
    >>> klaer = {
        'bukse': ['sokker', 'bokser'],
        'sokker': ['bokser'],
        'tskjorte': ['bukse'],
        'genser': ['bukse', 'tskjorte'],
        'klokke': ['genser'],
        'jakke': ['genser', 'sko'],
        'sko': ['bukse', 'tskjorte'],
        'bokser': [],
        'belte': ['bukse'],
        'morgenkaape': [],
        'hansker': ['jakke'],
    }
    >>> TopoSort = Topological_sort()
    >>> print TopoSort.sort(klaer, 'bukse')
    ['bukse', 'sokker', 'bokser'}
    """
    def dfs(self, graph, node):
        for child in graph[node]:
            if child not in self.discovered_nodes:
                self.dfs(graph, child)
                self.discovered_nodes.add(child)
        self.nodes_sorted.append(node)
        
    def sort(self, graph, start):
        self.nodes_sorted = []
        self.discovered_nodes = set()
        
        self.dfs(graph, start)
        self.nodes_sorted.reverse()
        return self.nodes_sorted

def calculate_path(graph, 

def relax(graph, node, d, pi):
    """Relaxes the edges from a node. Returns True if d was modified"""
    modified = False
    for child, weight in graph[node].iteritems():
        if d[node] + weight < d[child]:
            d[child] = d[node] + weight
            pi[child] = node
            modified = True
    return modified

def bellman_ford(graph, start):
    """
    Regner ut kostnadene ved å dra fra start til alle andre noder.
    d[v] = kostnaden for å dra til v
    pi[v] = forgjengeren til v i korteste-vei treet

    Om det finnes en negativ sykel kastes en Exception("Negative cycle detected")

    Eksempel på bruk:
    
    >>> graph = {
    'a': {'b': 8, 'c': 6, 'd': 7},
    'b': {'d': 10, 'e': 9},
    'c': {'d': 1, 'g': 1},
    'd': {'e': 3, 'f': 12, 'g': 3},
    'e': {'f': 11},
    'f': {'g': 9, 'h': 2},
    'g': {'h': 4},
    'h': {},
    }
    >>> print bellman_ford(graph, 'a')
    ({'c': 'a', 'b': 'a', 'e': 'd', 'd': 'a', 'g': 'c', 'f': 'd', 'h': 'g'},
     {'a': 0, 'c': 6, 'b': 8, 'e': 10, 'd': 7, 'g': 7, 'f': 19, 'h': 11})
    """
    
    inf = 10e3000
    d = {}
    pi = {}
    for v in graph:
        d[v] = inf
        pi[v] = None
    d[start] = 0

    # Calculate d and pi
    for n in range(1, len(graph)):
        for node in graph:
            relax(graph, node, d, pi)

    # Check for negative cycles
    for node in graph:
            if relax(graph, node, d, pi):
                raise Exception("Negative cycle detected")

    return pi, d

def DAG_shortest_path(graph, start):
    TopoSort = Topological_sort()

    # Gir f.eks. [a b c d e f g]
    sorted_nodes = TopoSort.sort(graph, start)

    # Initialize graph
    inf = 10e3000
    d = {}
    pi = {}
    for v in graph:
        d[v] = inf
        pi[v] = None
    d[start] = 0

    # Relax nodes in the correct order
    for v in sorted_nodes:
        relax(graph, v, d, pi)
        
    return pi, d

graph = {
    'a': {'b': 8, 'c': 6, 'd': 7},
    'b': {'d': 10, 'e': 9},
    'c': {'d': 1, 'g': 1},
    'd': {'e': 3, 'f': 12, 'g': 3},
    'e': {'f': 11},
    'f': {'g': 9, 'h': 2},
    'g': {'h': 4},
    'h': {},
}

print bellman_ford(graph, 'a') == DAG_shortest_path(graph, 'a')
