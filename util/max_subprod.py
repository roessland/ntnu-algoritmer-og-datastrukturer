"""
Finds the subarray with the highest product.

Running time: O(n)

Examples:
>>> max_subprod([0.1, 0.2, 0.3])
[0.3]
>>> max_subprod([0.1, 0.2, 0.3], True)
(2, 2)
>>> max_subprod([2.0, 3.0])
[2.0, 3.0]
>>> max_subprod([2.0, 3.0], True)
(0, 1)
>>> max_subprod([1.0, 2.0, 3.0])
[2.0, 3.0]
>>> max_subprod([1.0, 2.0, 3.0], True)
(1, 2)
>>> max_subprod([0.0, 50.0, 0.5, 50.0])
[50.0, 0.5, 50.0]
>>> max_subprod([0.0, 50.0, 0.5, 50.0], True)
(1, 3)
"""

def max_subprod(A, return_indices=False):
    max_i = 0
    max_j = 0
    maxprod = A[0]
    for j in range(1, len(A)):
        alternative = maxprod * A[j]
        if alternative > A[j]:
            maxprod *= A[j]
            max_j = j
        else:
            maxprod = A[j]
            max_i = j
            max_j = j
    if return_indices:
        return max_i, max_j
    else:
        return A[max_i:max_j+1]



if __name__ == '__main__':
    import doctest
    doctest.testmod()
