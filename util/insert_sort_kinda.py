# -*- coding: utf8 -*-
def insertSort(l):
    # Index of last element
    N = len(l) - 1
    finished = False

    # Empty, or list with a single element. We are done.
    if N == -1 or N == 0:
        finished = True
        
    while not finished:
        n = 0
        while l[N] > l[n]:
            n += 1
            if n == N:
                finished = True
                yield l
        if not finished:
            l.insert(n, l.pop())
            yield l

l = [1, 5, 6, 7, 2, 1, 5, 6]
for l in insertSort(l):
    print l
