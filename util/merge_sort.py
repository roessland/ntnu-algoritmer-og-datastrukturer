def merge(l, r):
    c = []
    while l or r:
        if l and r:
            if l[-1] > r[-1]:
                c.append(l.pop())
            else:
                c.append(r.pop())
        elif l:
            c.append(l.pop())
        elif r:
            c.append(r.pop())
    c.reverse()
    return c

def mergesort(l):
    n = len(l)
    if n <= 1:
        return l
    return merge(mergesort(l[:-n//2]), mergesort(l[n//2:]))

if __name__ == "__main__":
    from random import randint
    import timeit

    N = 100

    print "Sammenligner med innebygd sort..."

    setup = """
from __main__ import mergesort, merge
from random import randint
l = [randint(0, 1000000) for n in range(0, 10000)]"""
  
    t = timeit.Timer("mergesort(l)", setup)
    min = t.timeit(N)/N

    t = timeit.Timer("l.sort()", setup)
    tim = t.timeit(N)/N
    print "Ca.", min/tim, "ganger tregere"
