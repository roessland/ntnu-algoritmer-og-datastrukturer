class UF:
    """Implementation of weighted quick-union with path compression"""

    def __init__(self):
        """Initialize data structure"""
        self.id = {} # Use a dictionary, to support all data types
        self.sz = {} # Ditto. This contains the size of the tree at p
        self.count = 0

    def add(self, p):
        """Add site to data structure"""
        self.id[p] = p # A root site points to itself
        self.sz[p] = 1 # Component with a single site has size 1
        self.count += 1

    def union(self, p, q):
        """Add connection between p and q"""
        i = self.find(p)
        j = self.find(q)
        if i == j: return

        # Make smaller root point to larger one
        # This is the weighted union
        if self.sz[i] < self.sz[j]:
            self.id[i] = j
            self.sz[j] += self.sz[i]
        else:
            self.id[j] = i
            self.sz[i] += self.sz[j]

        self.count -= 1

    def find(self, p):
        """Return the root site of the component p belongs to"""
        # Copy the reference, so we don't change the input
        root = q = p 

        # Find the root node
        while root != self.id[root]:
            root = self.id[root]

        # Find it again, this time changing the pointers to root.
        # This is the path compression
        while q != root:
            q_copy = self.id[q]
            self.id[q] = root
            q = q_copy

        return root

    def connected(self, p, q):
        """Return true if p and q are in the same component"""
        return self.find(p) == self.find(q)

    def __repr__(self):
        return str(self.id.keys()) + "\n" + str(self.id.values())


if __name__ == '__main__':
    def timewrapper():
        """Bench the speed"""
        from random import randint
        graph = UF()
        sites = 100000
        connections = 100000
        tests = 100000

        # Create the sites
        for i in xrange(0, sites):
            graph.add(i)

        # Make random unions
        for a,b in [(randint(0, sites-1), randint(0, sites-1)) for n in xrange(connections)]:
            graph.union(a, b)

        # Check random connections
        for a,b in [(randint(0, sites-1), randint(0, sites-1)) for n in xrange(tests)]:
            graph.connected(a, b)
    #print "Running benchmark. This should take around 10 seconds on a 2012 CPU."
    #import timeit
    #t = timeit.Timer(timewrapper)
    #print t.timeit(10)

