def array_has_cycle(lst, x0):
    tortoise = lst[x0]
    hare = lst[lst[x0]]
    while tortoise != hare:
        tortoise = lst[tortoise]
        hare = lst[lst[hare]]

if __name__ == "__main__":
    a = [0,1,2,3,4,5,None]
    assert not array_has_cycle(a, 0)
