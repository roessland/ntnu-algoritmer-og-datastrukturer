from random import shuffle

def quicksort(l, shuffle_list=True):
    if not l:
        return l
    v = l[0]
    left = [n for n in l[1:] if n <= v]
    right = [n for n in l[1:] if n > v]
    return quicksort(left, False) + [v] + quicksort(right, False)
